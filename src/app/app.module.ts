import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RoutingModule, VerifyCity } from './routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HttpClientModule } from '@angular/common/http';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CityCardComponent } from './city-card/city-card.component';
import { DetailedWeatherComponent } from './detailed-weather/detailed-weather.component';

import { WeatherService } from './services/weather.service';
import { ConstantsService } from './services/contants.service';

@NgModule({
  declarations: [
    AppComponent,
    CityCardComponent,
    DetailedWeatherComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutingModule,
    FlexLayoutModule,
    MatCardModule,
    MatToolbarModule,
    HttpClientModule,
    MatProgressSpinnerModule
  ],
  providers: [WeatherService, ConstantsService, VerifyCity],
  bootstrap: [AppComponent]
})
export class AppModule { }
