import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { WeatherService } from '../services/weather.service';
import { concatMap, filter, switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-detailed-weather',
  templateUrl: './detailed-weather.component.html',
  styleUrls: ['./detailed-weather.component.css']
})
export class DetailedWeatherComponent implements OnInit {
  public city: String;
  /*
  Here the forecast data is result object and not an observable.
  */
  public forecastData: any;
  public isLoading: boolean;
  public displayError: boolean = true;
  constructor(private _route: ActivatedRoute, private _weatherService: WeatherService) { }

  ngOnInit(): void {
    this._route.paramMap.subscribe( paramMap => {
      this.displayError = false;
      this.isLoading = true;
      this.city = paramMap.get('cityName');
      if(this.city){
        /*
          Can use pipes and tap for loading but kept is simple.
        */
        this._weatherService.getForecastForCity(this.city).subscribe(
          res => {
            this.forecastData = res;
            this.isLoading = false;
          }, err => {
            this.displayError = true;
            this.isLoading = false;
          }
        );
      }
    });
  }

  /*
    Checks whether the timestamp equals to 9 AM
    @Input: timestamp
    @returns: true or false
  */
  public isItNineAM(timestamp: number): boolean {
    if (timestamp % (60*60*24) === 32400)
      return true;
    return false;
  }

}
