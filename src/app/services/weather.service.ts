import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ConstantsService } from './contants.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  constructor(private http: HttpClient, private constants: ConstantsService) {}

  /*
  Have used the exact api provided but could have added metrics param to ensure temperature in
  celsius. Its in Kelvin for now and am converting it in html
  */
  public getWeatherForCity(city: string): Observable<any> {
    const url = `${this.constants.APIURL}/weather?q=${city}&appid=${this.constants.AAPID}`;
    return this.http.get(url);
  }

  /*
    Was asked to get forecast for 5 days of weather at 9 AM. There is only one API for that but that 
    works on UTC time lika all others and gives data on 3 hour of granularity. As CET/CEST is +1/+2
    hours to UTC, I have assumed that UTC 9 AM should be fine.
  */
  public getForecastForCity(city: String): Observable<any> {
    const url = `${this.constants.APIURL}/forecast?q=${city}&appid=${this.constants.AAPID}`;
    return this.http.get(url);
  }
}
