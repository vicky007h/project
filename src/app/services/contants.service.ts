import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantsService {
  public readonly CITIES = ["Madrid", "Lisbon", "Bern", "London", "Paris"];
  public readonly AAPID = '3d8b309701a13f65b660fa2c64cdc517';
  public readonly APIURL = 'http://api.openweathermap.org/data/2.5';

  constructor() { }
}
