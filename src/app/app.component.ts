import { NgSwitchDefault } from '@angular/common';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ConstantsService } from './services/contants.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-weather-app';

  constructor(private _router: Router, public constants: ConstantsService) {}

  ngOnInit() {
    /* 
      Bit Extra :)
      Setting Background color of the app according to current time.
    */
    let hours = new Date().getHours();
    let bgColor = 'white';
    if (hours < 6 || hours > 21) //Night 9PM to 6AM the BG will be black
      bgColor = 'black';
    else if (hours > 6 && hours < 15) //6AM to 3PM the BG will be skyblue
      bgColor = 'skyblue';
    else // 3PM to 9PM the BG will be orangish shade for evening
      bgColor = '#f4a261';
    document.body.style.backgroundColor = bgColor;
  }

  public showDetails(city: String) {
    this._router.navigate([city]);
  }

  ngOnDestroy() {
  }
}
