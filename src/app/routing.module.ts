import { Injectable, NgModule } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterModule, RouterStateSnapshot, Routes, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { DetailedWeatherComponent } from './detailed-weather/detailed-weather.component';
import { ConstantsService } from './services/contants.service';
/*
  Moved the routing module from app.module for clarity.
*/

/*
* This function affirms that the app can only route to the cities we have provided.

  I have used guard which can be used for authentication and auhoriztion as well.
*/
@Injectable()
export class VerifyCity implements CanActivate {

    constructor(private constants: ConstantsService) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        return this.constants.CITIES.includes(route.paramMap.get('cityName'));
    }

}

const routes: Routes = [
    {
      path: "",
      component: DetailedWeatherComponent
    },
    {
      path: ":cityName",
      component: DetailedWeatherComponent,
      canActivate: [VerifyCity]
    }
  ];

@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })],
    exports: [
        RouterModule
    ]
})
export class RoutingModule { }