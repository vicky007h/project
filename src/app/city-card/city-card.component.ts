import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { WeatherService } from '../services/weather.service';

@Component({
  selector: 'city-card',
  templateUrl: './city-card.component.html',
  styleUrls: ['./city-card.component.css']
})
export class CityCardComponent implements OnInit {

  @Input() city: string;
  /*
  Here am using the weatherData as observable and the passing it to assync pipe in html.
  */
  public weatherData$: Observable<any>;
  public isLoading: boolean;
  public isError: boolean;

  constructor(private weatherService: WeatherService) {}

  ngOnInit() {
    this.isLoading = true;
    this.isError = false;
    this.weatherData$ = this.weatherService.getWeatherForCity(this.city);
    this.weatherData$.subscribe(
      (result) => {
        this.isLoading = false;
      }, (error) => {
        this.isLoading = false;
        this.isError = true;
      }
    )
  }

}
