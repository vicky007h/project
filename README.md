# AngularWeatherApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.7.

Its an App which displays weather information for 5 hardcoded cities. It use https://openweathermap.org/api for the weather data.
Inline comments have been provided for explanations and choices.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Testing

Please find the test cases documented in test-cases.xlsx

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
